#!/bin/bash

#DO NOT FORGET TO UPDATE THE SCRIPT REFERENCED IN THE BLOG POST:
#https://gitlab.com/ioiste/blog/-/blob/main/assets/2023-04-18-mega.io-backup-script/backup.sh

timestamp() {
  if [ "$1" == "date" ]; then
    date +"%d-%m-%Y"
  else
    date +"%d-%m-%Y-%H:%M:%S"
  fi
}

writeLog() {
  echo "$(timestamp): $1" 2>&1 | tee -a $BACKUP_GENERAL_LOG
}

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
DATA_TO_BACKUP="$1"
DATA_TO_BACKUP_PATH_HASH=$(echo -n "$DATA_TO_BACKUP" | md5sum | cut -d " " -f 1)
BACKUP_TMP="$SCRIPT_DIR/backup"
BACKUP_GENERAL_LOG=${BACKUP_TMP}/$(timestamp date)-backup.log
BACKUP_ARCHIVE=${BACKUP_TMP}/$(timestamp)-${DATA_TO_BACKUP_PATH_HASH}-data-backup.tgz
BACKUP_MEGA_PATH="/backups"
BACKUP_STATUS="Failed"
ENV_FILE="$SCRIPT_DIR/.env"

writeLog "Creating data dumps for databases."
MYSQL_DB=$(grep MYSQL_CONTAINER_NAME $ENV_FILE | cut -d '=' -f2)
PGSQL_DB=$(grep PGSQL_CONTAINER_NAME $ENV_FILE | cut -d '=' -f2)
declare -a dbContainers=("$MYSQL_DB" "$PGSQL_DB")
for key in "${dbContainers[@]}"; do
  writeLog "Dumping data for database $key"
  case $key in
  "$MYSQL_DB")
    # Install mysql-client https://serverfault.com/a/1048822/581041
    # mysqldump does not have to be the same version as the database
    MYSQL_PASSWORD=$(grep MYSQL_PASSWORD $ENV_FILE | cut -d '=' -f2)
    mysqldump -h $(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${key}) -u root -p"${MYSQL_PASSWORD}" --all-databases --single-transaction >${DATA_TO_BACKUP}/mysqldump.sql
    ;;
  "$PGSQL_DB")
    # Install the pg client: sudo apt-get install postgresql-client
    # In case of versions mismatch, you might need to get the official apt repository https://www.postgresql.org/download/linux/ubuntu/
    # Create the pgpass file: https://stackoverflow.com/a/50404176/5454684
    POSTGRES_USER=$(grep POSTGRES_USER $ENV_FILE | cut -d '=' -f2)
    pg_dumpall --clean -h $(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${key}) -U "${POSTGRES_USER}" -w >${DATA_TO_BACKUP}/pgsqldump.sql
    ;;
  *)
    writeLog "Unexpected database: $key"
    ;;
  esac

  if [ $? -eq 0 ]; then
    writeLog "Created data dump for $key."
  else
    writeLog "Failed to create data dump for $key."
  fi
done

writeLog "Archiving ${DATA_TO_BACKUP}."
sudo tar --exclude="${DATA_TO_BACKUP}/postgresql" --exclude="${DATA_TO_BACKUP}/mysql" -czf "${BACKUP_ARCHIVE}" ${DATA_TO_BACKUP}


if [ $? -eq 0 ]; then
  ARCHIVE_INFO=$(ls -lh "${BACKUP_ARCHIVE}")
  writeLog "Created ${BACKUP_ARCHIVE}. File information:"
  writeLog "${ARCHIVE_INFO}"
  writeLog "Uploading ${BACKUP_ARCHIVE} to cloud."
  # MegaCMD user guide: https://github.com/meganz/MEGAcmd/blob/master/UserGuide.md
  # You might need to login: mega-login --auth-code=XXXXX user@mail.com password
  mega-reload
  mega-put ${BACKUP_ARCHIVE} ${BACKUP_MEGA_PATH}

  if [ $? -eq 0 ]; then
    BACKUP_STATUS="Successful"
    writeLog "Upload complete."
    writeLog "Removing ${BACKUP_ARCHIVE} and database dumps."
    sudo rm ${BACKUP_ARCHIVE} ${DATA_TO_BACKUP}/mysqldump.sql ${DATA_TO_BACKUP}/pgsqldump.sql
    writeLog "Finished backup for ${DATA_TO_BACKUP}!"
  else
    writeLog "Upload to cloud for ${BACKUP_ARCHIVE} has failed."
  fi
else
  writeLog "Could not create backup file for ${DATA_TO_BACKUP}."
fi

MAILGUN_API_KEY=$(grep MAILGUN_API_KEY $ENV_FILE | cut -d '=' -f2)
MAILGUN_API_ENDPOINT=$(grep MAILGUN_API_ENDPOINT $ENV_FILE | cut -d '=' -f2)
MAILGUN_FROM_SENDER_NAME=$(grep MAILGUN_FROM_SENDER_NAME $ENV_FILE | cut -d '=' -f2)
MAILGUN_FROM_SENDER_EMAIL=$(grep MAILGUN_FROM_SENDER_EMAIL $ENV_FILE | cut -d '=' -f2)
MAILGUN_TO_EMAIL=$(grep MAILGUN_TO_EMAIL $ENV_FILE | cut -d '=' -f2)

if [[ -n "$MAILGUN_API_KEY" && -n "$MAILGUN_API_ENDPOINT" && -n "$MAILGUN_FROM_SENDER_NAME" && -n "$MAILGUN_FROM_SENDER_EMAIL" && -n "$MAILGUN_TO_EMAIL" ]]; then
  writeLog "Sending an email with operation logs."

  MAILGUN_LOG_FILE=${BACKUP_TMP}/mailgun-$(timestamp date).log
  RESPONSE_HTTP_CODE=$(curl --write-out "%{http_code}" --user "api:${MAILGUN_API_KEY}" ${MAILGUN_API_ENDPOINT} -F from="${MAILGUN_FROM_SENDER_NAME} <${MAILGUN_FROM_SENDER_EMAIL}>" -F to="${MAILGUN_TO_EMAIL}" -F subject="${BACKUP_STATUS} data backup $(timestamp date)" -F text="$(cat ${BACKUP_GENERAL_LOG})" --output >(cat >>${MAILGUN_LOG_FILE}) --silent)

  if [ "$RESPONSE_HTTP_CODE" != "200" ]; then
    writeLog "Failed to send email. Response with HTTP code $RESPONSE_HTTP_CODE. Full HTTP response in $MAILGUN_LOG_FILE."
    exit 2
  fi

  sudo rm ${MAILGUN_LOG_FILE}
fi
